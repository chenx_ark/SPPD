﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SysMisc
{
    class SimpleCodes
    {
        private void CalcFuncSpanTime()
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            /*其它代码*/
            sw.Stop();
            //sw.ElapsedMilliseconds;/*运行时间代码*/
        }


        TextBox tb_log_info = new TextBox();
        /*Log信息更新*/
        private void LogHelper_LogUpdateEvent(string _log)
        {
            _log += "\r\n";
            try
            {
                int textBoxLines = tb_log_info.Lines.Length;
                if (textBoxLines > 600)
                {
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                    sw.Start();
                    int delCount = 0;
                    StringBuilder builder;
                    builder = new StringBuilder(tb_log_info.Text);
                    string[] oldLines = tb_log_info.Lines;
                    for (int i = 0; i < 200; i++)
                    {/*统计待删除的200行的总长度*/
                        delCount += oldLines[i].Length;
                    }
                    builder.Remove(0, delCount);
                    builder.Append(_log);
                    sw.Stop();
                    InvokeHelper.Invoke(tb_log_info, "Clear", null);
                    InvokeHelper.Invoke(tb_log_info, "AppendText", builder.ToString());
                    //LogHelper.Debug(typeof(Form_Main), String.Format("更新TextBox耗时：{0} ms", sw.ElapsedMilliseconds));
                }
                else { InvokeHelper.Invoke(tb_log_info, "AppendText", _log); }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
    }
}
