﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysMisc
{
    public class AspectLogger : ILogger
    {
        public AspectLogger()
        {

        }

        void ILogger.Log(string msg)
        {
            LogHelper.Info(null, msg);
        }

        void ILogger.Log(string[] categories, string msg)
        {
            LogHelper.Info( null, string.Format( "[{0}] : {1}",string.Join(",",categories), msg));
        }

        void ILogger.LogException(Exception x)
        {
            LogHelper.Error(null, x);
        }
    }
}
