using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SysMisc
{
    public partial class frmSplash : Form,ISplashForm
    {
        public frmSplash()
        {
            InitializeComponent();

            string path_base = @"resources\img\";
            string path_full = path_base + "splash_icon.ico";
            if ( File.Exists( path_full ))
                this.Icon = new Icon(path_full);
            //this.Icon = new Icon(@"resources\img\splash_icon.ico");
            path_full = path_base + "splash_load.jpg";
            if (File.Exists(path_full))
                this.BackgroundImage = Image.FromFile(path_full);
            //this.BackgroundImage = Image.FromFile(@"resources\img\splash_load.jpg");
        }

        #region ISplashForm

        void ISplashForm.SetStatusInfo(string NewStatusInfo)
        {
            lbStatusInfo.Text = NewStatusInfo;
        }

        #endregion
    }
}