using System;
using System.Collections.Generic;
using System.Text;

namespace SysMisc
{
    /// <summary>
    /// interface for Splash Screen
    /// </summary>
    public interface ISplashForm
    {
        void SetStatusInfo(string NewStatusInfo);
    }
}
