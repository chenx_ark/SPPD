﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using log4net;
using log4net.Config;

namespace SysMisc
{
    public class LogHelper
    {
        public static void setGlobalContextProperties(string key, string value)
        {
            log4net.GlobalContext.Properties[key] = value;
        }

        /// <summary>
        /// 输出日志到Log4Net
        /// </summary>
        /// <param name="t"></param>
        /// <param name="ex"></param>
        #region
        public static void Fatal(Type t, Exception ex)
        {
            try
            {
                ILog log = LogManager.GetLogger("logfatal");
                log.Fatal("Fatal", ex);
                if (null != LogUpdateEvent)
                    LogUpdateEvent(LogType.FATAL, t, ex.ToString());
            }
            catch
            {
            }
        }
        public static void Error(Type t, Exception ex)
        {
            try
            {
                ILog log = LogManager.GetLogger("logerror");
                log.Error("Error", ex);
                if (null != LogUpdateEvent)
                    LogUpdateEvent(LogType.ERROR, t, ex.ToString());
            }
            catch
            {
            }
        }
        #endregion

        /// <summary>
        /// 输出日志到Log4Net
        /// </summary>
        /// <param name="t"></param>
        /// <param name="msg"></param>
        #region
        public static void Error(Type t, string msg)
        {
            try
            {
                ILog log = LogManager.GetLogger("logerror");
                log.Error(msg);
                if (null != LogUpdateEvent)
                    LogUpdateEvent(LogType.ERROR, t, msg);
            }
            catch
            {
            }
        }

        public static void Warn(Type t, string msg)
        {
            try
            {
                ILog log = LogManager.GetLogger("logwarn");
                log.Warn(msg);
                if (null != LogUpdateEvent)
                    LogUpdateEvent(LogType.WARN, t, msg);
            }
            catch
            {
            }
        }

        public static void Info( Type t, string msg)
        {
            try
            {
                ILog log = LogManager.GetLogger("loginfo");
                log.Info(msg);
                if (null != LogUpdateEvent)
                    LogUpdateEvent(LogType.INFO, t, msg);
            }
            catch
            {
            }
        }
        
        public static void Debug(Type t, string msg)
        {
            try
            {
                ILog log = LogManager.GetLogger("logdebug");
                log.Debug(msg);
                if (null != LogUpdateEvent)
                    LogUpdateEvent( LogType.DEBUG, t, msg);
            }
            catch
            {
            }
        }

        #endregion

        public delegate void LogUpdateDelegate( LogType typ,Type t, string log);
        public static event LogUpdateDelegate LogUpdateEvent;

        /// <summary>
        /// 暂时没实现
        /// </summary>
        /// <param name="lt"></param>
        public static void SetLogLevel( LogType lt)
        {
        }
        public enum LogType
        {
            DEBUG,
            INFO,
            WARN,
            ERROR,
            FATAL
        }
    }
}
